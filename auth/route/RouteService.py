# coding=utf-8
from auth.api.helpers import base_errors as errors
from auth.api.helpers import base_name as names
from auth.api.helpers.service import Gis as gs
from flask_restful import Resource, reqparse
from auth.api.src.Authentication import auth

class Service(Resource):
    def __init__(self):
        self.__parser = reqparse.RequestParser()
        self.__parser.add_argument('data')
        self.__args = self.__parser.parse_args()

    def parse_data(self):
        try:
            self.data = self.__args.get('data', None)
        except:
            return {names.ANSWER: errors.PARSE_DATA}

        try:
            self.data = gs.converter(self.data)
        except:
            return {names.ANSWER: errors.CONVERTER}
        return {names.ANSWER: errors.OK, names.DATA: self.data}

    def post(self):
        data = self.parse_data()
        if data.get(names.ANSWER, None) == errors.OK:
            answer = auth(data)
        else:
            answer = data
        return answer, errors.OK, {'Access-Control-Allow-Origin': '*'}

    def option(self):
        return "OK", errors.OK, {'Access-Control-Allow-Origin': '*'}